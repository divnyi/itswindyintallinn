## Setup

Run ```pod install``` in root directory.

Set Darksky API key in ```ItsWindyInTallinn/Core/Info.plist``` at ```API_KEY``` field.

## Mentions

Icon is designed by [smalllikeart](https://www.flaticon.com/authors/smalllikeart) at https://www.flaticon.com/free-icon/wind_1507000

## Author

Oleksii Horishnii, oleksii.horishnii@gmail.com
