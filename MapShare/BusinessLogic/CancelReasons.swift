//
//  Errors.swift
//  MapShare
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

enum CancelReasons: Error {
    case failedToInit
    case userCancelled
}
