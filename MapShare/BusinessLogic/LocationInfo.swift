//
//  LocationInfo.swift
//  MapShare
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct LocationInfo {
    var coordinates: Coordinates
    var locationName: String
}
