//
//  Constants.swift
//  MapShare
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class Constants: NSObject {
    static let locationNameNotFound = "Unknown"
    static let locationNameCurrent = "Your current location"
    static let waitOnFailSeconds = 2.0
}
