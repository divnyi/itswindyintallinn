//
//  MapkitLocationParser.swift
//  MapShare
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import MapKit

class MapkitLocationParser: NSObject {
    func parse(data: Data) -> LocationInfo? {
        guard let mapItem = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? MKMapItem else {
            return nil
        }
        if mapItem.isCurrentLocation {
            // If this MKMapItem represents your current location (isCurrentLocation == YES), then placemark will be nil.
            return nil
        }
        let placemark: MKPlacemark = mapItem.placemark
        let lat = placemark.coordinate.latitude
        let lon = placemark.coordinate.longitude
        let name = placemark.title
        return LocationInfo(coordinates: Coordinates(lat: lat,
                                                     lon: lon),
                            locationName: name ?? Constants.locationNameNotFound)
    }
}
