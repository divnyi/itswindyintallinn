//
//  LocationProcessor.swift
//  MapShare
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class LocationFinder: NSObject {
    static func run(extensionContext: NSExtensionContext,
                    callback: @escaping (Result<LocationInfo, Void>) -> Void) {
        let vcardParser = VcardLocationParser()
        let mapkitParser = MapkitLocationParser()
        let extensionContextProcessor = ExtensionContextProcessor<LocationInfo>(extensionContext: extensionContext)
        
        extensionContextProcessor.run(attachmentParsers: [
            "public.vcard" : vcardParser.parse,
            "com.apple.mapkit.map-item" : mapkitParser.parse,
            ], callback: callback)
    }
}
