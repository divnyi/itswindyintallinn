//
//  VcardLocationParser.swift
//  MapShare
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Contacts

class VcardLocationParser: NSObject {
    func parse(data: Data) -> LocationInfo? {
        if let coordinates = parseCoordinates(data: data) {
            let locationName = parseLocationName(data: data) ?? Constants.locationNameNotFound
            return LocationInfo(coordinates: coordinates, locationName: locationName)
        }
        return nil
    }

    func parseLocationName(data: Data) -> String? {
        if let contacts = try? CNContactVCardSerialization.contacts(with: data) {
            if let contact = contacts.first {
                if contact.givenName == "My Location" {
                    return Constants.locationNameCurrent
                }
                if let adresses = contact.postalAddresses.first?.value {
                    let result = [
                        adresses.street,
                        [adresses.city, adresses.state, adresses.postalCode]
                            .filter { $0.count > 0 }
                            .joined(separator: ", "),
                        adresses.country,
                        ].joined(separator: "\n")
                    return result
                }
            }
        }
        return nil
    }
    
    func parseCoordinates(data: Data) -> Coordinates? {
        guard let vcardRaw = String(data: data, encoding: String.Encoding.utf8) else {
            return nil
        }
        let stringCoords = vcardRaw.capturedGroups(withRegex: "ll=([\\.\\-0-9]+)\\\\,([\\.\\-0-9]+)")
        guard stringCoords.count == 2,
            let lat = Double(stringCoords[0]),
            let lon = Double(stringCoords[1]) else {
                return nil
        }
        return Coordinates(lat: lat, lon: lon)
    }
}
