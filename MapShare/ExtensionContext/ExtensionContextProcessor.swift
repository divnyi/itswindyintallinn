//
//  CoordinateParser.swift
//  MapShare
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class ExtensionContextProcessor<T> {
    var extensionContext: NSExtensionContext
    init(extensionContext: NSExtensionContext) {
        self.extensionContext = extensionContext
    }
    
    func run(attachmentParsers: [String: (Data) -> T?],
             callback: @escaping (Result<T, Void>) -> Void) {
        var didSucceed = false
        var loadingStarted = 0
        let context: NSExtensionContext = self.extensionContext
        for item in context.inputItems {
            if let item = item as? NSExtensionItem {
                for attachment in item.attachments ?? [] {
                    for identifier in attachment.registeredTypeIdentifiers {
                        if let parser = attachmentParsers[identifier] {
                            loadingStarted += 1
                            loadAttachment(attachment: attachment,
                                           identifier: identifier)
                            { data in
                                if didSucceed { return }
                                if let result = parser(data) {
                                    didSucceed = true
                                    callback(.success(result))
                                } else {
                                    loadingStarted -= 1
                                    if loadingStarted == 0 {
                                        callback(.failure(()))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if loadingStarted == 0 {
            callback(.failure(()))
        }
    }
    
    private func loadAttachment(attachment: NSItemProvider,
                        identifier: String,
                        onSuccess: @escaping (Data) -> Void) {
        attachment.loadItem(
            forTypeIdentifier: identifier,
            options: [:]) { (item, error) in
                DispatchQueue.main.async {
                    guard let data = item as? Data else {
                        return
                    }
                    onSuccess(data)
                }
        }
    }
}
