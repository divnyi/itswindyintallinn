//
//  Resolver.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Swinject

class Resolver {
    static let shared = Resolver()

    let container = Container()
    
    init() {
        self.setup()
    }

    public func setup() {
        self.container.register(CoordinatesSetterVM.self, factory: { _ in CoordinatesSetterVMImpl() })
        
        self.container.register(KeyValueStorage.self, factory: { _ in KeyValueStorageImpl() })
            .inObjectScope(.container) // singleton
    }
}
