//
//  CoordinatesSetterVM.swift
//  MapShare
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Observable

enum LoadingState {
    case loading
    case loaded(LocationInfo)
    case failed
}

protocol CoordinatesSetterVM {
    var state: Observable<LoadingState> { get }
    func setContext(_ context: NSExtensionContext)
    func closeClicked()
    func submitClicked()
}

class CoordinatesSetterVMImpl: CoordinatesSetterVM {
    var state = Observable(LoadingState.loading)
    
    var context: NSExtensionContext?
    func setContext(_ context: NSExtensionContext) {
        self.context = context
        LocationFinder.run(extensionContext: context) { result in
            switch result {
            case .success(let locationInfo):
                self.state.value = .loaded(locationInfo)
            case .failure(_):
                self.state.value = .failed
                DispatchQueue.main.asyncAfter(deadline: .now() + Constants.waitOnFailSeconds) {
                    context.cancelRequest(withError: CancelReasons.failedToInit)
                }
            }
        }
    }
    
    func closeClicked() {
        self.context?.cancelRequest(withError: CancelReasons.userCancelled)
    }
    
    func submitClicked() {
        switch self.state.value {
        case .loaded(let locationInfo):
            let storedCoordinates = StoredCoordinates(defaults: Coordinates(lat: 0.0, lon: 0.0))
            storedCoordinates.lat.value = locationInfo.coordinates.lat
            storedCoordinates.lon.value = locationInfo.coordinates.lon
            
            self.context?.completeRequest(returningItems: nil, completionHandler: nil)
        default:
            break;
        }
    }
}
