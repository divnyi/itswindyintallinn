//
//  CoordinateSetterVC.swift
//  MapShare
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import MapKit
import Contacts

class CoordinateSetterVC: BaseVC {
    @IBOutlet weak var containerView: UIView!

    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var lat: UILabel!
    @IBOutlet weak var lon: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    @IBOutlet weak var submitButton: UIButton!

    let vm = Resolver.shared.container.resolve(CoordinatesSetterVM.self)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.containerView.layer.cornerRadius = 20.0
        self.containerView.clipsToBounds = true
        
        self.vm.setContext(self.extensionContext!)
    }
    
    override func subscribe() {
        super.subscribe()
        
        self.vm.state.observe { (state, _) in
            self.address.text = ""
            self.lat.text = ""
            self.lon.text = ""
            self.submitButton.isEnabled = false
            self.activityIndicator.isHidden = true

            switch state {
            case .loading:
                self.activityIndicator.isHidden = false
            case .failed:
                self.address.text = "Failed :("
                let halfDissmissSeconds = Constants.waitOnFailSeconds/2.0
                Utils.afterDelay(seconds: halfDissmissSeconds, fn: {
                    UIView.animate(withDuration: halfDissmissSeconds, animations: {
                        self.view.alpha = 0.0
                    })
                })
            case .loaded(let locationInfo):
                self.address.text = locationInfo.locationName
                self.lat.text = "Latitude: \(locationInfo.coordinates.lat)"
                self.lon.text = "Longitude: \(locationInfo.coordinates.lon)"
                self.submitButton.isEnabled = true
            }
        }.add(to: &self.disposal)
    }
    
    @IBAction func closeClicked(_ sender: Any) {
        self.vm.closeClicked()
    }
    
    @IBAction func submitClicked(_ sender: Any) {
        self.vm.submitClicked()
    }
}
