//
//  GradientView.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class GradientView: UIView {
    @IBInspectable
    var fromColor: UIColor = UIColor(red: 230.0/255.0, green: 244.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    @IBInspectable
    var toColor: UIColor = UIColor(red: 170.0/255.0, green: 184.0/255.0, blue: 225.0/255.0, alpha: 1.0)

    override open class var layerClass: AnyClass {
        return CAGradientLayer.classForCoder()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let gradientLayer = layer as! CAGradientLayer
        gradientLayer.colors = [fromColor.cgColor, toColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.2, y: 0.4)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
    }
}
