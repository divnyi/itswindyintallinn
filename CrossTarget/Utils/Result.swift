//
//  Result.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

enum Result<Success, Failure> {
    case success(_ result: Success)
    case failure(_ reason: Failure)
}
