//
//  Utils.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class Utils: NSObject {
    public class func createVC<Type>(storyboardId: String, vcId: String? = nil) -> Type {
        let type = Type.self
        let bundle = Bundle(for: type as! AnyClass)
        let storyboard = UIStoryboard.init(name: storyboardId, bundle: bundle)
        let vcId = vcId ?? String(describing: Type.self)
        let vc = storyboard.instantiateViewController(withIdentifier: vcId)
        
        return vc as! Type
    }
    
    public class func afterDelay(seconds: Double, fn: @escaping (() -> ())) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            fn()
        }
    }
}
