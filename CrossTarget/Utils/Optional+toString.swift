//
//  Optional+toString.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension Optional {
    func toString(nilValue: String) -> String {
        switch self {
        case .none:
            return nilValue
        case .some(let val):
            return String(describing: val)
        }
    }
}
