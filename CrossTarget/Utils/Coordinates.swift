//
//  Coordinates.swift
//  MapShare
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct Coordinates {
    var lat: Double
    var lon: Double
}
