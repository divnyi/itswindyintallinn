//
//  Log.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

enum LogType: String {
    case network
    case fetch
    case capabilities
    case coordinates
    case customUrl
}

func logWrite(_ string: String, level: Int, type: LogType, file: String = #file, line: Int = #line, function: String = #function) {
    Resolver.shared.container.resolve(Log.self)?.write(string, level: level, type: type.rawValue, file: file, line: line, function: function)
}

protocol Log {
    func write(_ string: String, level: Int, type: String, file: String, line: Int, function: String)
}

class LogImpl: Log {
    func write(_ string: String, level: Int, type: String, file: String = #file, line: Int = #line, function: String = #function) {
        print("[\(type):\(level)]\nin \(function)\nat \((file as NSString).lastPathComponent):\(line)\n\(string)\n")
    }
}
