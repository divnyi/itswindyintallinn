//
//  Coordinates.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct StoredCoordinates {
    var lat: StoredObject<Double>
    var lon: StoredObject<Double>
    
    init(defaults: Coordinates) {
        lat = StoredObject<Double>(key: "lat", defaultValue: defaults.lat)
        lon = StoredObject<Double>(key: "lon", defaultValue: defaults.lon)
    }
    
    var coordinates: Coordinates {
        return Coordinates(lat: lat.value, lon: lon.value)
    }
}
