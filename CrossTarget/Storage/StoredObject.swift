//
//  StoredObject.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class StoredObject<T> {
    var key: String
    var value: T {
        didSet {
            let kvs = Resolver.shared.container.resolve(KeyValueStorage.self)!
            kvs.store(key: self.key, value: self.value)
        }
    }
    init(key: String, defaultValue: T) {
        let kvs = Resolver.shared.container.resolve(KeyValueStorage.self)!
        self.key = key
        self.value = kvs.get(key: key) as? T ?? defaultValue
    }
}
