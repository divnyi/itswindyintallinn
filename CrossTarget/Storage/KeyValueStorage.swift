//
//  KeyValueStorage.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol KeyValueStorage {
    func store(key: String, value: Any?)
    func get(key: String) -> Any?
}

class KeyValueStorageImpl: KeyValueStorage {
    let sharedDefaults = UserDefaults.init(suiteName: "group.ItsWindyInTallinn")

    func store(key: String, value: Any?) {
        if value == nil {
            self.sharedDefaults?.removeObject(forKey: key)
        } else {
            self.sharedDefaults?.set(value, forKey: key)
        }
    }
    
    func get(key: String) -> Any? {
        return self.sharedDefaults?.object(forKey: key)
    }
}
