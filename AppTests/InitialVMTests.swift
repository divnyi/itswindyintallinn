//
//  InitialVMTests.swift
//  AppTests
//
//  Created by Oleksii Horishnii on 7/29/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable import ItsWindyInTallinn

import XCTest
import Observable
import Swinject

class InitialVMTests: XCTestCase {
    static let sampleWeather = Weather(time: 123456,
                                summary: "OK",
                                temperature: 25.0,
                                windSpeed: 5.4,
                                windBearing: 90)
    class MockedFetchUC: FetchUC {
        var lastWeather: Observable<Weather?> = Observable(nil)
        var coordinates: Coordinates = Coordinates(lat: 10.0, lon: 20.0)
        func fetch(callback: ((UIBackgroundFetchResult) -> Void)?) {
            self.lastWeather.value = sampleWeather
        }
    }
    class MockedCapabilities: Capabilities {
        var badges = Observable(false)
        var backgroundFetch = Observable(false)
        
        func setup() { }
    }
    
    let fetchUC = MockedFetchUC()
    let capabilities = MockedCapabilities()
    override func setUp() {
        Resolver.shared.setup()
        Resolver.shared.container.register(FetchUC.self, factory: { _ in self.fetchUC })
        Resolver.shared.container.register(Capabilities.self, factory: { _ in self.capabilities })
    }
    
    func testInitialState() {
        let vm = Resolver.resolve(InitialVM.self)
        XCTAssertEqual(vm.isLoading.value, true)
        XCTAssertEqual(vm.temperature.value, "unknown")
        XCTAssertEqual(vm.windSpeed.value, "unknown")
        XCTAssertEqual(vm.lastUpdate.value, "Last update: never :(")

        XCTAssertEqual(vm.lat.value, "lat: 10.0000")
        XCTAssertEqual(vm.lon.value, "lon: 20.0000")

        XCTAssertEqual(vm.lacksCapabilities.value, """
Background App Refresh is disabled
Notification Badges are disabled
-> Click here to fix <-
""")
    }
    
    func testAfterFetch() {
        let vm = Resolver.resolve(InitialVM.self)
        
        self.fetchUC.fetch { _ in }
        
        XCTAssertEqual(vm.isLoading.value, false)
        XCTAssertEqual(vm.temperature.value, "25.0 °C")
        XCTAssertEqual(vm.windSpeed.value, "540 cm\nE")
        XCTAssertEqual(vm.lastUpdate.value, "Last update: 0s ago")
        
        XCTAssertEqual(vm.lat.value, "lat: 10.0000")
        XCTAssertEqual(vm.lon.value, "lon: 20.0000")
        
        XCTAssertEqual(vm.lacksCapabilities.value, """
Background App Refresh is disabled
Notification Badges are disabled
-> Click here to fix <-
""")
    }
}
