//
//  ApiTests.swift
//  DarkskyTests
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable import ItsWindyInTallinn

import XCTest

class ApiTests: XCTestCase {
    let api = Resolver.shared.container.resolve(DarkskyAPI.self)!
    func testForecast() {
        let exp = expectation(description: "api call succeed")
        api.forecastRequest(coordinates: Coordinates(lat: 0.0, lon: 0.0),
                             callback: { (result) in
                                switch result {
                                case .success(_):
                                    exp.fulfill()
                                default:
                                    XCTFail("failed - \(result)")
                                }
        })
        waitForExpectations(timeout: 2.0, handler: nil)
    }
}
