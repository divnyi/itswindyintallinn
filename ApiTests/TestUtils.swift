//
//  Utils.swift
//  DarkskyTests
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class TestUtils: NSObject {
    static func getJsonData(_ name: String) -> Data {
        guard let pathString = Bundle(for: TestUtils.self).path(forResource: name, ofType: "json") else {
            fatalError("\(name).json not found")
        }
        guard let jsonString = try? String(contentsOfFile: pathString, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) else {
            fatalError("Unable to convert \(name).json to String")
        }
        guard let jsonData = jsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) else {
            fatalError("Unable to convert UnitTestData.json to NSData")
        }
        return jsonData
    }
}
