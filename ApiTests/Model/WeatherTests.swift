//
//  darkskyTests.swift
//  darkskyTests
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

@testable import ItsWindyInTallinn

import XCTest

class WeatherTests: XCTestCase {
    func testDecoding() {
        let sampleOutput = TestUtils.getJsonData("forecast")
        guard let forecast = try? JSONDecoder().decode(Forecast.self, from: sampleOutput) else {
            XCTFail("forecast failed to parse")
            return
        }
        XCTAssertEqual(forecast.currently.time, 1564225326)
        XCTAssertEqual(forecast.currently.summary, "Clear")
        XCTAssertEqual(forecast.currently.temperature, 82.07)
        XCTAssertEqual(forecast.currently.windBearing, 47)
    }
}
