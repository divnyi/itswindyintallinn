//
//  Weather.swift
//  darksky
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct Weather: Codable, CustomStringConvertible {
    let time: Int
    let summary: String
    let temperature: Double
    let windSpeed: Double
    let windBearing: Int
    
    var date: Date {
        return Date(timeIntervalSince1970: TimeInterval(self.time))
    }
    var windDirection: String {
        let directions = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
        var adjustedAngle = self.windBearing + 360/(directions.count*2)
        adjustedAngle = max(adjustedAngle, 0)
        adjustedAngle = adjustedAngle % 360
        let i: Int = Int(adjustedAngle / (360/directions.count))
        return directions[i % 8]
    }
    var windSpeedCentimeteres: Int {
        return Int((self.windSpeed*100).rounded())
    }
    
    var description: String {
        return "<Weather: time=\(time), summary=\(summary), temperature=\(temperature), windSpeed=\(windSpeed), windBearing=\(windBearing), windDirection=\(windDirection), date=\(date)>"
    }
}
