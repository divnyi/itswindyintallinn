//
//  Forecast.swift
//  Darksky
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

struct Forecast: Codable, CustomStringConvertible {
    let currently: Weather
    
    var description: String {
        return "<Forecast: currently=\(currently)>"
    }
}
