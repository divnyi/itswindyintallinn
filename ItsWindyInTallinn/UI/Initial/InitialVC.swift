//
//  ViewController.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Observable

class InitialVC: BaseVC {
    static func create() -> InitialVC {
        return Utils.createVC(storyboardId: "Initial")
    }
    
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var lastUpdateLabel: UILabel!

    @IBOutlet weak var noCapabilityButton: UIButton!
    @IBOutlet weak var loadingOverlay: UIView!
    
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lonLabel: UILabel!

    let vm = Resolver.resolve(InitialVM.self)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.style()
    }
    
    func style() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.noCapabilityButton.titleLabel?.textAlignment = .center
        self.noCapabilityButton.titleLabel?.minimumScaleFactor = 0.5
        self.loadingOverlay.layoutMargins = UIEdgeInsets(top: 500, left: 8, bottom: 8, right: 8)
    }

    override func subscribe() {
        super.subscribe()
        
        self.vm.isLoading.observe { (isLoading, _) in
            if isLoading {
                self.loadingOverlay.alpha = 1.0
            } else {
                UIView.animate(withDuration: 0.4, animations: {
                    self.loadingOverlay.alpha = 0.0
                })
            }
        }.add(to: &self.disposal)
        
        self.vm.windSpeed.observe { (windSpeed, _) in
            self.windSpeedLabel.text = windSpeed
        }.add(to: &self.disposal)
        
        self.vm.temperature.observe { (temperature, _) in
            self.temperatureLabel.text = temperature
        }.add(to: &self.disposal)
        
        self.vm.lastUpdate.observe { (lastUpdate, _) in
            self.lastUpdateLabel.text = lastUpdate
        }.add(to: &self.disposal)
        
        self.vm.lacksCapabilities.observe { (lacksMessage, _) in
            if let message = lacksMessage {
                self.noCapabilityButton.isHidden = false
                self.noCapabilityButton.setTitle(message, for: .normal)
            } else {
                self.noCapabilityButton.isHidden = true
            }
        }.add(to: &self.disposal)
        
        self.vm.lat.observe { (lat, _) in
            self.latLabel.text = lat
        }.add(to: &self.disposal)
        
        self.vm.lon.observe { (lon, _) in
            self.lonLabel.text = lon
        }.add(to: &self.disposal)
    }
    
    @IBAction func noBadgesButtonClick(_ sender: Any) {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
    }
}

