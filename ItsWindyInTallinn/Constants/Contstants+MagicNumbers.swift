//
//  Contstants+MagicNumbers.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

extension Constants {
    class MagicNumbers: NSObject {
        static let minimumFetchInterval = 60.0 // seconds
        static let tallinnCoordinates = Coordinates(lat: 59.4370, lon: 24.7536)
    }
}
