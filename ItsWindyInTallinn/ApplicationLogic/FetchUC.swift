//
//  FetchUC.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Observable

protocol FetchUC {
    var lastWeather: Observable<Weather?> { get }
    var coordinates: Coordinates { get }
    
    func fetch(callback: ((UIBackgroundFetchResult) -> Void)?)
}

class FetchUCImpl: FetchUC {
    var lastWeather: Observable<Weather?> = Observable(nil)

    var coordinates: Coordinates {
        let storedCoordinates = StoredCoordinates(defaults: Constants.MagicNumbers.tallinnCoordinates)
        let coordinates = storedCoordinates.coordinates
        logWrite("coordinates = \(coordinates)\ntallinCoordinates = \(Constants.MagicNumbers.tallinnCoordinates)", level: 5, type: .coordinates)
        return coordinates
    }
    
    let api = Resolver.shared.container.resolve(DarkskyAPI.self)!

    func fetch(callback: ((UIBackgroundFetchResult) -> Void)?) {
        self.api.forecastRequest(coordinates: self.coordinates) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let forecast):
                    self.lastWeather.value = forecast.currently
                    callback?(.newData)
                default:
                    callback?(.failed)
                }
            }
        }
    }
}
