//
//  RawNetwork.swift
//  Darksky
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol RawNetwork {
    func get(url: URL, callback: @escaping (Result<RawNetworkImpl.RawResponse, Error?>) -> Void)
}

class RawNetworkImpl: RawNetwork {
    struct RawResponse {
        let httpCode: Int
        let data: Data?
    }
    
    func get(url: URL, callback: @escaping (Result<RawResponse, Error?>) -> Void) {
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.urlCache = nil
        
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: url) {
            (data, response, error) in
            guard error == nil,
                let httpCode = (response as? HTTPURLResponse)?.statusCode else {
                callback(.failure(error))
                return
            }
            let response = RawResponse(httpCode: httpCode,
                                       data: data)
            callback(.success(response))
            return
        }
        task.resume()
    }
}
