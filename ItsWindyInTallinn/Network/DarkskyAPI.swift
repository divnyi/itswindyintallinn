//
//  DarkskyAPI.swift
//  darksky
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol DarkskyAPI {
    func forecastRequest(coordinates: Coordinates,
                         callback: @escaping (Result<Forecast, DarkskyAPIImpl.ErrorType>) -> Void)
}

class DarkskyAPIImpl: DarkskyAPI {
    enum ErrorType {
        case rawNetworkError(_ reason: Error?)
        case statusCode(_ data: RawNetworkImpl.RawResponse)
        case parsingError
    }
    
    let rawNetwork = Resolver.shared.container.resolve(RawNetwork.self)!

    func forecastRequest(coordinates: Coordinates,
                         callback: @escaping (Result<Forecast, ErrorType>) -> Void) {
        let url = DarkskyURL.forecast(coordinates: coordinates)

        self.rawNetwork.get(url: url) { response in
            switch response {
            case .failure(let reason):
                logWrite("network error == \(reason.toString(nilValue: "nil"))", level: 0, type: .network)
                callback(.failure(.rawNetworkError(reason)))
            case .success(let rawResponse):
                guard rawResponse.httpCode == 200 else {
                    logWrite("status code == \(rawResponse.httpCode)", level: 0, type: .network)
                    callback(.failure(.statusCode(rawResponse)))
                    return
                }
                guard let data = rawResponse.data,
                    let forecast = try? JSONDecoder().decode(Forecast.self, from: data) else {
                        logWrite("parsing error", level: 0, type: .network)
                        callback(.failure(.parsingError))
                        return
                }
                logWrite("forecast = \(forecast)", level: 5, type: .network)
                callback(.success(forecast))
            }
        }
    }
}
