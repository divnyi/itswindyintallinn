//
//  DarkskyURL.swift
//  Darksky
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class DarkskyURL: NSObject {
    static let baseURL: String = "api.darksky.net"
    static let apiKey: String = {
        if let apiKey = Bundle(for: DarkskyURL.self).object(forInfoDictionaryKey: "API_KEY") as? String,
            apiKey.count > 0 {
            return apiKey
        } else {
            fatalError("Set Darksky API key in ItsWindyInTallinn/Core/Info.plist at API_KEY field")
        }
    }()
    
    static func forecast(coordinates: Coordinates) -> URL {
        var components = URLComponents()
        components.scheme = "https"
        components.host = DarkskyURL.baseURL
        components.path = "/forecast/\(DarkskyURL.apiKey)/\(coordinates.lat),\(coordinates.lon)"
        components.queryItems = [
            URLQueryItem(name: "exclude", value: "[minutely,hourly,daily,alerts,flags]"),
            URLQueryItem(name: "lang", value: "en"),
            URLQueryItem(name: "units", value: "si"),
        ]
        return components.url!
    }
}
