//
//  Capabilities.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import UserNotifications
import Observable

protocol Capabilities {
    var badges: Observable<Bool> { get }
    var backgroundFetch: Observable<Bool> { get }
    func setup()
}

class CapabilitiesImpl: Capabilities {
    var badges = Observable(false)
    var backgroundFetch = Observable(false)
    
    func setup() {
        UNUserNotificationCenter.current().requestAuthorization(options: .badge) { (granted, error) in
            DispatchQueue.main.async {
                self.badges.value = granted
            }
        }
        
        DispatchQueue.main.async {
            UIApplication.shared.setMinimumBackgroundFetchInterval(Constants.MagicNumbers.minimumFetchInterval)
            self.backgroundFetch.value = UIApplication.shared.backgroundRefreshStatus == .available
        }
    }
}
