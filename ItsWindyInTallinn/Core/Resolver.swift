//
//  Resolver.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Swinject

class Resolver {
    static let shared = Resolver()

    let container = Container()
    
    init() {
        self.setup()
    }
    
    public static func resolve<Service>(_ serviceType: Service.Type) -> Service {
        return Resolver.shared.container.resolve(serviceType)!
    }

    public func setup() {
        self.container.register(Capabilities.self, factory: { _ in CapabilitiesImpl() })
            .inObjectScope(.container) // singleton

        self.container.register(RawNetwork.self, factory: { _ in RawNetworkImpl() })
        self.container.register(DarkskyAPI.self, factory: { _ in DarkskyAPIImpl() })

        self.container.register(InitialVM.self, factory: { _ in InitialVMImpl() })

        self.container.register(FetchUC.self, factory: { _ in FetchUCImpl() })
            .inObjectScope(.container) // singleton

        self.container.register(Log.self, factory: { _ in LogImpl() })
            .inObjectScope(.container) // singleton
        
        self.container.register(KeyValueStorage.self, factory: { _ in KeyValueStorageImpl() })
            .inObjectScope(.container) // singleton
    }
}
