//
//  AppDelegate.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Observable

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var appCoordinator: ApplicationCoordinator = ApplicationCoordinator()
    var fetchUC: FetchUC = Resolver.shared.container.resolve(FetchUC.self)!
    
    var disposal: Disposal = []

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.appCoordinator.start()
        
        self.fetchUC.lastWeather.observe { (weather, _) in
            if let weather = weather {
                let windSpeed = weather.windSpeedCentimeteres
                logWrite("windSpeed is \(windSpeed)", level: 5, type: .fetch)
                UIApplication.shared.applicationIconBadgeNumber = windSpeed
            } else {
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
        }.add(to: &disposal)
        
        return true
    }
    
    func application(_ application: UIApplication,
                     performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        self.fetchUC.fetch { result in
            logWrite("result = \(result.rawValue)", level: result == .failed ? 0 : 5, type: .fetch)
            completionHandler(result)
        }
    }
}

