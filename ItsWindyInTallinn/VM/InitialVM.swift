//
//  InitialVM.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/28/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit
import Observable

protocol InitialVM {
    var isLoading: Observable<Bool> { get }
    var windSpeed: Observable<String> { get }
    var temperature: Observable<String> { get }
    var lastUpdate: Observable<String> { get }
    
    var lat: Observable<String> { get }
    var lon: Observable<String> { get }

    var lacksCapabilities: Observable<String?> { get }
}

class InitialVMImpl: InitialVM {
    var isLoading = Observable(true)
    var windSpeed = Observable("unknown")
    var temperature = Observable("unknown")
    var lastUpdate = Observable("Last update: never :(")

    var lat = Observable("")
    var lon = Observable("")

    var lacksCapabilities = Observable<String?>(nil)

    let capabilities = Resolver.shared.container.resolve(Capabilities.self)!
    let fetchUC = Resolver.shared.container.resolve(FetchUC.self)!
    
    var disposal: Disposal = []

    var enterForegroundSubscription: NSObjectProtocol? = nil
    
    init() {
        self.fetchUC.lastWeather.observe { [weak self] (weather, _) in
            if let weather = weather {
                self?.updateWeather(weather: weather)
            }
        }.add(to: &self.disposal)
        
        self.capabilities.badges.observe { [weak self] (_, _) in
            self?.updateCapability()
        }.add(to: &self.disposal)
        self.capabilities.backgroundFetch.observe { [weak self] (_, _) in
            self?.updateCapability()
        }.add(to: &self.disposal)
        
        self.enterForegroundSubscription =
            NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification,
                                                   object: nil,
                                                   queue: nil) { [weak self] _ in
                                                    self?.onEnteringForeground()
        }
        self.onEnteringForeground()
    }

    deinit {
        if let observer = self.enterForegroundSubscription {
            NotificationCenter.default.removeObserver(observer)
        }
    }

    func onEnteringForeground() {
        self.startLastUpdate() // restart Timer after BG
        self.capabilities.setup()
        let coordinates = self.fetchUC.coordinates
        self.lat.value = String(format: "lat: %.4f", coordinates.lat)
        self.lon.value = String(format: "lon: %.4f", coordinates.lon)
    }
    
    func updateWeather(weather: Weather) {
        self.isLoading.value = false
        self.windSpeed.value = "\(weather.windSpeedCentimeteres) cm\n\(weather.windDirection)"
        self.temperature.value = "\(weather.temperature) °C"
        self.lastUpdate.value = "Last update: 0s ago"
        self.startLastUpdate()
    }
    
    var timer: Timer?
    func startLastUpdate() {
        guard let date = self.fetchUC.lastWeather.value?.date else {
            return
        }
        var sinceSeconds = Int(Date().timeIntervalSince(date))
        self.timer?.invalidate()
        
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] _ in
            self?.lastUpdate.value = "Last update: \(sinceSeconds)s ago"
            sinceSeconds += 1
        })
    }
    
    func updateCapability() {
        logWrite("backgroundFetch allowed: \(self.capabilities.backgroundFetch.value)\nbadges allowed: \(self.capabilities.badges.value)", level: 5, type: .capabilities)
        if self.capabilities.backgroundFetch.value &&
            self.capabilities.badges.value {
            self.lacksCapabilities.value = nil
            return
        }
        var message = ""
        message += self.capabilities.backgroundFetch.value ? "" : "Background App Refresh is disabled\n"
        message += self.capabilities.badges.value ? "" : "Notification Badges are disabled\n"
        message += "-> Click here to fix <-"
        self.lacksCapabilities.value = message
    }
}
