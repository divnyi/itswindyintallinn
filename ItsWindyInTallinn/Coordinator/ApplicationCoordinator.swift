//
//  ApplicationCoordinator.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

class ApplicationCoordinator: Coordinator {
    let vc: InitialVC
    let window: UIWindow
    let rootNVC: UINavigationController
    
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds)) {
        self.window = window
        self.vc = InitialVC.create()
        self.rootNVC = UINavigationController(rootViewController: self.vc)
    }
    
    func start() {
        self.window.rootViewController = self.rootNVC
        self.window.makeKeyAndVisible()
    }
}
