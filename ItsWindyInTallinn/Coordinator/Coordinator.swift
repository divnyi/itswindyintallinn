//
//  Coordinator.swift
//  ItsWindyInTallinn
//
//  Created by Oleksii Horishnii on 7/27/19.
//  Copyright © 2019 Oleksii Horishnii. All rights reserved.
//

import UIKit

protocol Coordinator {
    func start()
}
